#!/usr/bin/env python3


#
# This is the project on-boarding script for Taiga
#
# It's purpose is to perform the tasks necessary to put a new project into Taiga
#
# Service ticket: https://jira.ncsa.illinois.edu/browse/SVC-3145
#
# Tasks:
#  1) Creation of directory
#  2) Set quota
#  3) Assign owner:group
#  4) Ensure owner:group exists
#  6) assign certain dirs to MDTs

# working_config_file structure:
#
# default_fs : <fs>
# fs:
#      fs1: <base_path>
#      fs2: <base_path>
# projects:
#      project1:
#          <relative path>
#          <quota>
#      project2:
#          <relative path>
#          <quota>
#
import sys

import click
import yaml
import subprocess
import os
from os import path

# config_file = "/etc/settool.conf"
config_file = "/home/pehartma/git/project_onboard/etc/settool.conf"
working_dir = ""
working_config_file = "settool_wrk.conf"
working_abs = working_dir + "/" + working_config_file


@click.group(invoke_without_command=True)
def cli():
    check_setup()
    report()


@cli.command()
@click.pass_context
def report():
    pass


@cli.command()
@click.pass_context
def add_fs(fs_name, abs_path):
    working_file = load_working_file()
    if working_file is None:
        output_dict = {
            'default_fs': fs_name,
            'FS': {
                fs_name: abs_path
            }
        }
        write_yaml(working_abs, output_dict)
    else:
        if working_file['FS'].get(fs_name) is None:
            working_file['FS'][fs_name] = abs_path
            write_yaml(working_abs, working_file)


@cli.command()
@click.pass_context
def change_default_fs(fs_name):
    working_file = load_working_file()
    if working_file is None:
        print("Please add the FS before making it the default")
    if working_file['FS'].get(fs_name) is None:
        print(f"FS {fs_name} not found in the configuration file. "
              f"Please add the FS before making it the default.")
    else:
        working_file['default_fs'] = fs_name
        write_yaml(working_abs, working_file)


@cli.command()
@click.pass_context
def add_project(project_name, project_rel_path, project_quota, fs=None, mdt=None):
    working_file = load_working_file()
    if working_file is None:
        print("Cannot add a project until a FS is defined")
        exit()
    else:
        if fs is None:
            fs_path = working_file['FS'][working_file['default_fs']]
        elif working_file['FS'].get(fs) is None:
            print(f"FS {fs} not found in configuration file."
                  f"Please ad the FS before adding projects")
            exit()
        else:
            fs_path = working_file['FS'].get(fs)
        good_rel_path = get_good_path(fs_path, project_rel_path)
        working_file['projects'] = {project_name:
                                    {"rel_path": good_rel_path,
                                     "quota": project_quota}
                                    }
        write_yaml(working_abs, working_file)
    full_path = fs_path + "/" + good_rel_path
    if not os.path.exists(full_path):
        if mdt is None:
            mdt = get_balanced_mdt()
        run_command = f"lfs mkdir -i {mdt} {full_path}"
        subprocess.run(sys.executable, "-c", run_command)


def get_balanced_mdt():
    popen_command = "lfs df |grep MDT | awk '{print $5 " " $6}'|sed 's/%//'"
    output = subprocess.run(sys.executable, "-c", popen_command)
    highest = 0
    chosen_mdt = 0
    for perc, mdt in output:
        if perc > highest:
            chosen_mdt = mdt
    chosen_mdt = chosen_mdt.split(':')[1].strip(']')
    return chosen_mdt

def get_good_path(fs_path, project_rel_path):
    if fs_path in project_rel_path:
        return os.path.relpath(project_rel_path, fs_path)
    else:
        return project_rel_path


def write_yaml(file, output_dict):
    try:
        with open(file, "r") as output_fp:
            yaml.safe_dump(output_dict, output_fp)
    except EnvironmentError as EE:
        print(f"write_yaml received error '{EE}'")


def load_working_file():
    load_path = working_dir + "/" + working_config_file
    if path.exists(load_path):
        with open(load_path, "r") as lp_fp:
            loaded = yaml.safe_load(lp_fp)
            if "default_fs" not in loaded.keys():
                return None
            else:
                return loaded
    else:
        return None


def get_settings():
    settings = []
    settings["working_dir"] = "Not Set"
    msg = "Where is the working directory?"
    settings["working_dir"] = input(msg)
    if not path.exists(settings["working_dir"]):
        msg = "Path " + settings["working_dir"] + " not found. Create? {y/n}"
        inp_create_path = input(msg)
        if inp_create_path.lower() == "y":
            os.makedirs(settings["working_dir"])
    working_cfg = settings["working_dir"] + "/" + working_config_file
    if not path.exists(working_cfg):
        with open(working_cfg, "w") as wrk_fp:
            wrk_fp.write("")

    return settings


def setup_setup():
    global working_dir
    global working_abs
    msg = "File " + config_file + " not found. Create? (y/n)"
    response = input(msg)
    if response.lower() == "y":
        with open(config_file, mode="w") as cf_fp:
            settings = get_settings()
            if "working_dir" not in settings:
                print("Entry incorrect")
                exit()
            else:
                data = {"working_dir": settings["working_dir"]}
                yaml.dump(data, cf_fp)
                working_dir = settings["working_dir"]
                working_abs = working_dir + "/" + working_config_file


def check_setup():
    global working_dir
    global working_abs
    if path.exists(config_file):
        with open(config_file, mode="r") as conf_fp:
            try:
                conf_yaml = yaml.safe_load(conf_fp)
            except yaml.YAMLError as exc:
                print(exc)
    else:
        setup_setup()
        if path.exists(config_file):
            with open(config_file, mode="r") as conf_fp:
                try:
                    conf_yaml = yaml.safe_load(conf_fp)
                except yaml.YAMLError as exc:
                    print(exc)
        else:
            print("file creation incorrect")
            exit()

    if "working_dir" not in conf_yaml.keys():
        setup_setup()
    else:
        working_dir = conf_yaml["working_dir"]
        working_abs = working_dir + "/" + working_config_file


if __name__ == '__main__':
    cli()
