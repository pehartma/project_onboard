#!/usr/bin/env python2
import shlex
# from pathlib import Path
import os
from subprocess import Popen, PIPE
# import subprocess

default_project_map = "project_map"
DRYRUN = False
VERBOSE = False


class OnboardTool:
    def __init__(self, projectfile):
        self.basepath = None
        self.project_list = []
        self.fs_name = None
        self.projectfile = projectfile

    def load_project_map(self):
        if VERBOSE:
            print("Starting load_project_map")
        with open(self.projectfile, 'r') as prjfile:
            for line in prjfile.readlines():
                if VERBOSE:
                    print("line[0] = \"{}\"".format(line[0]))
                if not line[0].isalnum():
                    continue
                else:
                    if VERBOSE:
                        print("line = {}".format(line))
                    prj_id = line.split()[0]
                    quota_tb = line.split()[1]
                    prj_path = line.split()[2]
                    self.project_list.append({'project_id': prj_id, 'quota': quota_tb, 'prj_path': prj_path,
                                              'use': True})

    def verify_prj_ids(self):
        if VERBOSE:
            print("Starting verify_prj_ids")
        for count, project in enumerate(self.project_list):
            project_id = project['project_id']
            process = Popen(["getent", "group", project_id], stdout=PIPE)
            (output, err) = process.communicate()
            exit_code = process.wait()

            if exit_code != 0:
                print("Project ID {} not found in getent".format(project_id))
                self.project_list[count]["use"] = False
            else:
                self.project_list[count]["GID"] = output.split(':')[2]

    def get_balanced_mdt(self):
        if VERBOSE:
            print("Starting get_balanced_mdt")

        command = "lfs df |grep MDT | awk '{print $5 " " $6}'|sed 's/%//'"
        popen_command = shlex.split(command)
        process = Popen(popen_command, stdout=PIPE)
        (output, err) = process.communicate()
        exit_code = process.wait()

        if exit_code != 0:
            print("command {} exited with code {}, err: {}".format(command, exit_code, err))
        highest = 0
        chosen_mdt = 0
        for perc, mdt in output:
            if perc > highest:
                chosen_mdt = mdt
        chosen_mdt = chosen_mdt.split(':')[1].strip(']')
        return chosen_mdt

    def set_quota(self, project_id, quota_tb, directory):
        if VERBOSE:
            print("Starting set_quota")

        quota_inode = int(quota_tb) * 625000
        quota = int(quota_tb) * 1073741824
        command = "lfs setquota -p {} -b {} -B {} -i {} -I {} {}".format(project_id, quota, quota, quota_inode,
                                                                         quota_inode, directory)
        if VERBOSE:
            print("set_quota: Command to run is {}".format(command))
        popen_command = shlex.split(command)
        if VERBOSE:
            print("set_quota: shlexed command is {}".format(popen_command))
        if DRYRUN:
            print("set_quota: command to run: {}".format(command))
        else:
            if VERBOSE:
                print("set_quota: Running {}".format(command))
            process = Popen(popen_command, stdout=PIPE)
            (output, err) = process.communicate()
            exit_code = process.wait()
            if exit_code != 0:
                print("command '{}' exited with code {}, error output {}".format(popen_command, exit_code, err))
            if VERBOSE:
                print("set_quota: command output {}".format(output))

    def get_fs(self, directory):
        if VERBOSE:
            print("Starting get_fs")

        return "/{}".format(directory.split('/')[1])

    def create_directory(self, project_count):
        if VERBOSE:
            print("Starting create_directory")

        project_id = self.project_list[project_count]["GID"]
        quota_tb = self.project_list[project_count]["quota"]
        directory = self.project_list[project_count]["prj_path"]

        if not os.path.exists(directory):
            mdt = self.get_balanced_mdt()
            command = "lfs mkdir -i {} {}".format(mdt, directory)
            if DRYRUN:
                print("create_directory: command to run: {}".format(command))
            else:
                popen_command = shlex.split(command)
                process = Popen(popen_command, stdout=PIPE)
                (output, err) = process.communicate()
                exit_code = process.wait()

                if exit_code != 0:
                    print("command '{}' exited with code {}, error output {}".format(popen_command, exit_code, err))

        os.chown(directory, 0, 0)

        self.set_quota(project_id, quota_tb, self.get_fs(directory))

    def check_quota(self, project_count):
        if VERBOSE:
            print("Starting check_quota")

        command = "lfs quota -q -p {} {}".format(self.project_list[project_count]["GID"],
                                                 self.get_fs(self.project_list[project_count]["prj_path"]))
        popen_command = shlex.split(command)
        process = Popen(popen_command, stdout=PIPE)
        (output, err) = process.communicate()
        exit_code = process.wait()

        if exit_code != 0:
            print("command '{}' exited with code {}, error output {}".format(popen_command, exit_code, err))
        set_value = output.split()[3]
        listed_value = int(self.project_list[project_count]["quota"])
        listed_value = listed_value * 1073741824
        if VERBOSE:
            print("set_value = {}, listed_value = {}".format(set_value, listed_value))
            print("output of {} is {}".format(command, output))
        if not DRYRUN:
            if set_value != listed_value:
                self.set_quota(self.project_list[project_count]["GID"], int(self.project_list[project_count]["quota"]),
                               self.get_fs(self.project_list[project_count]["prj_path"]))


def run_tool(project_map=default_project_map):
    if VERBOSE:
        print("Starting run_tool")

    tool = OnboardTool(project_map)

    if VERBOSE:
        print("running tool.load_project_map")
    tool.load_project_map()
    if VERBOSE:
        print("running tool.verify_prj_ids")
    tool.verify_prj_ids()
    for count, project_id in enumerate(tool.project_list):
        if tool.project_list[count]["use"] is True:
            if not os.path.exists(tool.project_list[count]["prj_path"]):
                if VERBOSE:
                    print("running tool.create_directory({})".format(count))
                tool.create_directory(count)
            if VERBOSE:
                print("running tool.check_quota({})".format(count))
            tool.check_quota(count)


if __name__ == '__main__':
    run_tool("/taiga/admin/taiga-config/project_map")
